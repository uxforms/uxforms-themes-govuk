var gulp = require('gulp'),
    runSequence = require('run-sequence'),
    pkg = require('../../package.json'),
    bump = require('gulp-bump'),
    config = require('../config'),
    fs = require('fs'),
    awspublish = require('gulp-awspublish'),
    rename = require('gulp-rename');

gulp.task('release', function(d) {
    return runSequence(
        'clean',
        'package',
        'publish',
        'bump'
    );
});

gulp.task('publish', function() {
    var publisher = awspublish.create({
        region: "eu-west-2",
        params: {
            Bucket: "artifacts-public.uxforms.net"
        }
    });
    var artifactName = pkg.name + '-' + pkg.version + '.zip';
    return gulp.src(config.TARGET_DIR + '/' + artifactName)
      .pipe(rename('/com/uxforms/' + pkg.name + '/' + pkg.version + '/' + artifactName))
      .pipe(publisher.publish({}))
      .pipe(publisher.cache())
      .pipe(awspublish.reporter())

});


gulp.task('bump', function() {
    return gulp.src('./package.json')
        .pipe(bump({type:'minor'}))
        .pipe(gulp.dest('./'));
});
